import csv

FILENAME = "Harry Potter.csv"

coordinates = [
['x: ', -25, -21, 0, 14, -4, 1, 1, -22, 1, 2] ,
['y: ', 14, -11, -8, -8, 15, -6, 13, 11, 0, -2]
]

with open(FILENAME, "w", encoding='utf-8', newline="") as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(coordinates)